NUNEZA,ERNEST JAN BSIT-3
	
a. List the books Authored by Marjorie Green
- The Busy Executive's Database Guide
- You can Combat Computer Stress!
b. List the books Authored by Micheal O'Leary
- Cooking with Computers
- Sushi, Anyone
c. Write the author/s of "The Busy Executives Database Guide"
-Marjorie Green
d. Identify the publisher of "But Is It User Friendly?"
-Algodata Infosystems	
e. List the books published by Algodata Infosystems
-But Is It User Friendly?
-Secrets of Silicon Valley
-Net Etiquette
-The Busy Executive's Database Guide
-Cooking with Computers
-Straight Talk About Computers

-------------------------------------------------------------------------------------



Microsoft Windows [Version 10.0.19044.2604]
(c) Microsoft Corporation. All rights reserved.

C:\Users\ej nuneza>mysql -u root
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 136
Server version: 10.4.27-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE DATABASE blog_db;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]> USE blog_db;
Database changed
MariaDB [blog_db]> CREATE TABLE post (id INT NOT NULL AUTO_INCREMENT, author_id INT NOT NULL, content VARCHAR(500), datetime_posted DATETIME, PRIMARY KEY (id));
Query OK, 0 rows affected (0.005 sec)

MariaDB [blog_db]> ALTER TABLE post RENAME TO posts;
Query OK, 0 rows affected (0.005 sec)

MariaDB [blog_db]> SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| posts             |
+-------------------+
1 row in set (0.000 sec)

MariaDB [blog_db]> CREATE TABLE users (id INT NOT NULL AUTO_INCREMENT, email VARCHAR(100) NOT NULL, password VARCHAR(300) NOT NULL, datetime_created DATETIME, PRIMARY KEY (id));
Query OK, 0 rows affected (0.007 sec)

MariaDB [blog_db]> DROP TABLE users;
Query OK, 0 rows affected (0.004 sec)

MariaDB [blog_db]> CREATE TABLE users (id INT NOT NULL AUTO_INCREMENT, email VARCHAR(100) NOT NULL, password VARCHAR(300) NOT NULL, datetime_created DATETIME, PRIMARY KEY (id));
Query OK, 0 rows affected (0.005 sec)

MariaDB [blog_db]> DROP TABLE posts;
Query OK, 0 rows affected (0.003 sec)

MariaDB [blog_db]> CREATE TABLE posts (id INT NOT NULL AUTO_INCREMENT, author_id INT NOT NULL, content VARCHAR(500), datetime_posted DATETIME, PRIMARY KEY (id), CONSTRAINT fk_author_id FOREIGN KEY(author_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT);
Query OK, 0 rows affected (0.011 sec)

MariaDB [blog_db]> DESCRIBE posts;
+-----------------+--------------+------+-----+---------+----------------+
| Field           | Type         | Null | Key | Default | Extra          |
+-----------------+--------------+------+-----+---------+----------------+
| id              | int(11)      | NO   | PRI | NULL    | auto_increment |
| author_id       | int(11)      | NO   | MUL | NULL    |                |
| content         | varchar(500) | YES  |     | NULL    |                |
| datetime_posted | datetime     | YES  |     | NULL    |                |
+-----------------+--------------+------+-----+---------+----------------+
4 rows in set (0.005 sec)

MariaDB [blog_db]>  CREATE TABLE post_comments ( id INT NOT NULL AUTO_INCREMENT, post_id INT NOT NULL,  user_id INT NOT NULL, content VARCHAR(5000) NOT NULL,datetime_commented DATETIME, PRIMARY KEY (id), CONSTRAINT fk_post_id FOREIGN KEY (post_id) REFERENCES posts(id) ON UPDATE CASCADE ON DELETE RESTRICT, CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT);
Query OK, 0 rows affected (0.012 sec)

MariaDB [blog_db]> SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| post_comments     |
| posts             |
| users             |
+-------------------+
3 rows in set (0.000 sec)

MariaDB [blog_db]> CREATE TABLE post_likes ( id INT NOT NULL AUTO_INCREMENT, post_id INT NOT NULL,  user_id INT NOT NULL,datetime_liked DATETIME, PRIMARY KEY (id), CONSTRAINT fk_post_likes_post_id FOREIGN KEY (post_id) REFERENCES posts(id) ON UPDATE CASCADE ON DELETE RESTRICT, CONSTRAINT fk_post_likes_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT);
Query OK, 0 rows affected (0.011 sec)

MariaDB [blog_db]> DESCRIBE TABLE posts;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'TABLE posts' at line 1
MariaDB [blog_db]> DESCRIBE posts;
+-----------------+--------------+------+-----+---------+----------------+
| Field           | Type         | Null | Key | Default | Extra          |
+-----------------+--------------+------+-----+---------+----------------+
| id              | int(11)      | NO   | PRI | NULL    | auto_increment |
| author_id       | int(11)      | NO   | MUL | NULL    |                |
| content         | varchar(500) | YES  |     | NULL    |                |
| datetime_posted | datetime     | YES  |     | NULL    |                |
+-----------------+--------------+------+-----+---------+----------------+
4 rows in set (0.005 sec)

MariaDB [blog_db]> DROP TABLE posts;
ERROR 1451 (23000): Cannot delete or update a parent row: a foreign key constraint fails
MariaDB [blog_db]> ALTER TABLE posts ADD title VARCHAR (500);
Query OK, 0 rows affected (0.006 sec)
Records: 0  Duplicates: 0  Warnings: 0

MariaDB [blog_db]> SHOW posts;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'posts' at line 1
MariaDB [blog_db]> DESCRIBE posts;
+-----------------+--------------+------+-----+---------+----------------+
| Field           | Type         | Null | Key | Default | Extra          |
+-----------------+--------------+------+-----+---------+----------------+
| id              | int(11)      | NO   | PRI | NULL    | auto_increment |
| author_id       | int(11)      | NO   | MUL | NULL    |                |
| content         | varchar(500) | YES  |     | NULL    |                |
| datetime_posted | datetime     | YES  |     | NULL    |                |
| title           | varchar(500) | YES  |     | NULL    |                |
+-----------------+--------------+------+-----+---------+----------------+
5 rows in set (0.005 sec)

MariaDB [blog_db]> SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| post_comments     |
| post_likes        |
| posts             |
| users             |
+-------------------+
4 rows in set (0.000 sec)